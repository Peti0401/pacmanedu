package com.gyarmatip.pacmanedu.algorithmobjects;

import com.gyarmatip.pacmanedu.utils.Utils;
import javafx.beans.property.SimpleStringProperty;

import java.lang.reflect.Field;
import java.util.List;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.stream.Collectors;

public class Algorithm {

    private SimpleStringProperty name = new SimpleStringProperty(this, "name");
    private SimpleStringProperty description = new SimpleStringProperty(this, "description");
    private SimpleStringProperty algorithmSteps = new SimpleStringProperty(this, "algorithmSteps");
    private SimpleStringProperty actionDescription = new SimpleStringProperty(this, "actionDescription");
    private SimpleStringProperty resultInterpretation = new SimpleStringProperty(this, "resultInterpretation");
    private SimpleStringProperty furtherUses = new SimpleStringProperty(this, "furtherUses");
    private SimpleStringProperty testYourKnowledgeDescription = new SimpleStringProperty(this, "testYourKnowledgeDescription");

    public Algorithm(ResourceBundle resourceBundle) {
        List<SimpleStringProperty> stringProperties = getAllProperties();
        stringProperties.forEach(stringProperty ->
        {
            String value = resourceBundle.getString(stringProperty.getName());
            stringProperty.set(value);
        });
    }

    public Algorithm(String bundleBaseName, Locale locale) {
        this(ResourceBundle.getBundle(bundleBaseName, locale));
    }

    public String getName() {
        return name.get();
    }

    public SimpleStringProperty nameProperty() {
        return name;
    }

    public String getDescription() {
        return description.get();
    }

    public SimpleStringProperty descriptionProperty() {
        return description;
    }

    public String getAlgorithmSteps() {
        return algorithmSteps.get();
    }

    public SimpleStringProperty algorithmStepsProperty() {
        return algorithmSteps;
    }

    public String getActionDescription() {
        return actionDescription.get();
    }

    public SimpleStringProperty actionDescriptionProperty() {
        return actionDescription;
    }

    public String getResultInterpretation() {
        return resultInterpretation.get();
    }

    public SimpleStringProperty resultInterpretationProperty() {
        return resultInterpretation;
    }

    public String getFurtherUses() {
        return furtherUses.get();
    }

    public SimpleStringProperty furtherUsesProperty() {
        return furtherUses;
    }

    public String getTestYourKnowledgeDescription() {
        return testYourKnowledgeDescription.get();
    }

    public SimpleStringProperty testYourKnowledgeDescriptionProperty() {
        return testYourKnowledgeDescription;
    }

    public List<SimpleStringProperty> getAllProperties() {
        List<Field> fields = Utils.ReflectionUtil.getFields(this);
        return fields.stream()
                .map(field -> Utils.ReflectionUtil.callPropertyGetter(field, this))
                .filter(o -> o instanceof SimpleStringProperty)
                .map(o -> (SimpleStringProperty)o)
                .collect(Collectors.toList());
    }

    public String[] getSingleAlgorithmSteps() {
        return getAlgorithmSteps().split("\n");
    }

}
