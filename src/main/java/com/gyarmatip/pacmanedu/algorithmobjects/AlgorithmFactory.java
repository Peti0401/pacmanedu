package com.gyarmatip.pacmanedu.algorithmobjects;

import com.gyarmatip.pacmanedu.Main;
import com.gyarmatip.pacmanedu.utils.Utils;

import java.lang.reflect.Field;
import java.util.List;
import java.util.stream.Collectors;

public class AlgorithmFactory {

    public static final Algorithm DIJKSTRA = new Algorithm("com.gyarmatip.pacmanedu.bundles.algorithms.alg_dijkstra", Main.usedLocale);
    public static final Algorithm EULERIAN = new Algorithm("com.gyarmatip.pacmanedu.bundles.algorithms.alg_eulerian", Main.usedLocale);
    //public static final Algorithm HAMILTONIAN = new Algorithm("com.gyarmatip.pacmanedu.bundles.algorithms.alg_hamiltonian", Main.usedLocale);
    public static final Algorithm KRUSKAL = new Algorithm("com.gyarmatip.pacmanedu.bundles.algorithms.alg_kruskal", Main.usedLocale);

    public static List<Algorithm> getDeclaredAlgorithms() {
        AlgorithmFactory factoryInstance = new AlgorithmFactory();
        List<Field> fields = Utils.ReflectionUtil.getFields(factoryInstance);
        return fields.stream()
                .filter(field -> field.getType().equals(Algorithm.class))
                .map(field -> {
                    try {
                        return ((Algorithm) field.get(factoryInstance));
                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    }
                    return null;
                })
                .collect(Collectors.toList());
    }

}
