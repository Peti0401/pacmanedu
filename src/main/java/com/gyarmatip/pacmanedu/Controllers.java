package com.gyarmatip.pacmanedu;

import com.gyarmatip.pacmanedu.ui.SplashViewController;
import com.gyarmatip.pacmanedu.ui.main.BoardViewController;
import com.gyarmatip.pacmanedu.ui.main.MainViewController;
import com.gyarmatip.pacmanedu.ui.sections.AdvancedAlgorithmsViewController;
import com.gyarmatip.pacmanedu.ui.sections.GraphBuilderViewController;
import com.gyarmatip.pacmanedu.ui.sections.GraphFundamentalsViewController;

/**
 * Collects the controllers of the FXML documents
 */
public class Controllers {

    public static SplashViewController splashViewController;
    public static MainViewController mainViewController;

    public static BoardViewController boardViewController;

    public static AdvancedAlgorithmsViewController advancedAlgorithmsViewController;
    public static GraphBuilderViewController graphBuilderViewController;
    public static GraphFundamentalsViewController graphFundamentalsViewController;

}
