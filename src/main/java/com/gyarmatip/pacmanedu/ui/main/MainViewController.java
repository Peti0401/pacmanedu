package com.gyarmatip.pacmanedu.ui.main;

import com.gyarmatip.pacmanedu.Controllers;
import com.gyarmatip.pacmanedu.Main;
import com.gyarmatip.pacmanedu.ui.sections.AdvancedAlgorithmsViewController;
import com.gyarmatip.pacmanedu.ui.sections.GraphBuilderViewController;
import com.gyarmatip.pacmanedu.ui.sections.GraphFundamentalsViewController;
import com.gyarmatip.pacmanedu.utils.UTF8Control;
import com.gyarmatip.pacmanedu.utils.Utils;
import com.jfoenix.controls.JFXSnackbar;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.TitledPane;
import javafx.scene.layout.StackPane;
import javafx.util.Pair;

import java.net.URL;
import java.util.ResourceBundle;

public class MainViewController implements Initializable {

    @FXML private StackPane root;
    @FXML private StackPane boardRoot;

    @FXML private TitledPane graphFundamentalsTitledPane;
    @FXML private TitledPane advancedAlgorithmsTitledPane;
    @FXML private TitledPane graphBuilderTitledPane;

    private JFXSnackbar snackbar;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        snackbar = new JFXSnackbar(root);
        snackbar.setPrefWidth(350);

        // board section
        Pair<Node, Initializable> boardPair =
                Utils.loadFXML("BoardView.fxml", BoardViewController.class);
        Controllers.boardViewController = ((BoardViewController) boardPair.getValue());
        boardRoot.getChildren().add(boardPair.getKey());
        System.out.println("BoardView loaded");

        ResourceBundle fundamentalsBundle = ResourceBundle.getBundle(
                "com.gyarmatip.pacmanedu.bundles.fundamentals",
                Main.usedLocale,
                new UTF8Control());
        // controls section
        Pair<Node, Initializable> graphFundamentalsPair =
                Utils.loadFXML("GraphFundamentalsView.fxml", GraphFundamentalsViewController.class, fundamentalsBundle);
        Controllers.graphFundamentalsViewController = ((GraphFundamentalsViewController) graphFundamentalsPair.getValue());
        graphFundamentalsTitledPane.setContent(graphFundamentalsPair.getKey());
        System.out.println("GraphFundamentalsView loaded");

        ResourceBundle advancedAlgorithmsBundle = ResourceBundle.getBundle(
                "com.gyarmatip.pacmanedu.bundles.advancedAlgorithms",
                Main.usedLocale,
                new UTF8Control());
        Pair<Node, Initializable> advancedAlgorithmsPair =
                Utils.loadFXML("AdvancedAlgorithmsView.fxml", AdvancedAlgorithmsViewController.class, advancedAlgorithmsBundle);
        Controllers.advancedAlgorithmsViewController = ((AdvancedAlgorithmsViewController) advancedAlgorithmsPair.getValue());
        advancedAlgorithmsTitledPane.setContent(advancedAlgorithmsPair.getKey());
        System.out.println("AdvancedAlgorithmsView loaded");

        ResourceBundle graphBuilderBundle = ResourceBundle.getBundle(
                "com.gyarmatip.pacmanedu.bundles.graphBuilder",
                Main.usedLocale,
                new UTF8Control());
        Pair<Node, Initializable> graphBuilderPair =
                Utils.loadFXML("GraphBuilderView.fxml", GraphBuilderViewController.class, graphBuilderBundle);
        Controllers.graphBuilderViewController = ((GraphBuilderViewController) graphBuilderPair.getValue());
        graphBuilderTitledPane.setContent(graphBuilderPair.getKey());
        System.out.println("GraphBuilderView loaded");

    }

    public JFXSnackbar getSnackbar() {
        return snackbar;
    }
}
