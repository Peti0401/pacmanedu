package com.gyarmatip.pacmanedu.ui.main;

import com.gyarmatip.boardapi.board.Board;
import com.gyarmatip.boardapi.board.BoardAnimator;
import com.gyarmatip.boardapi.board.BoardStructure;
import com.gyarmatip.boardapi.board.tile.Tile;
import com.gyarmatip.boardapi.board.tile.TileType;
import com.gyarmatip.boardapi.navigation.pathfinding.DijkstrasPathFinder;
import de.jensd.fx.glyphs.materialdesignicons.MaterialDesignIcon;
import de.jensd.fx.glyphs.materialdesignicons.MaterialDesignIconView;
import javafx.beans.binding.DoubleExpression;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.MenuItem;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Tab;

public class BoardControllerTab extends Tab {

    private Board board;
    private DijkstrasPathFinder dijkstrasPathFinder;
    private ContextMenu contextMenu;
    private ObservableList<Tile> markedTiles = FXCollections.observableArrayList();
    private SimpleBooleanProperty isInMarkMode = new SimpleBooleanProperty(false);

    public BoardControllerTab(String boardName, Board board, DijkstrasPathFinder dijkstrasPathFinder) {
        super(boardName);
        this.board = board;
        this.dijkstrasPathFinder = dijkstrasPathFinder;

        //wrap in scroll pane
        ScrollPane scrollPane = new ScrollPane();
        scrollPane.getStyleClass().setAll("board-container");
        scrollPane.setContent(board);
        super.setContent(scrollPane);

        // create context menu
        MenuItem markStart = new MenuItem("", new MaterialDesignIconView(MaterialDesignIcon.PIN));
        MenuItem markEnd = new MenuItem("", new MaterialDesignIconView(MaterialDesignIcon.FLAG_CHECKERED));

        markStart.setOnAction(event ->
        {
            Tile tile = ((Tile) markStart.getParentPopup().getOwnerNode());
            tile.markAsStartTile();
            markStart.setDisable(true);
            markEnd.setDisable(false);
            markedTiles.add(tile);
        });
        markEnd.setOnAction(event ->
        {
            Tile tile = ((Tile) markStart.getParentPopup().getOwnerNode());
            tile.markAsEndTile();
            markStart.setDisable(false);
            markEnd.setDisable(true);
            markedTiles.add(tile);
        });
        markEnd.setDisable(true);
        contextMenu = new ContextMenu(markStart, markEnd);

        board.getTiles().forEach(tile ->
                tile.setOnContextMenuRequested(event ->
                {
                    if (dijkstrasPathFinder.isVertex(board.getPosition(tile)) && isInMarkMode.get()) {
                        contextMenu.show(tile, event.getScreenX(), event.getScreenY());
                    }
                }));

        markedTiles.addListener((ListChangeListener<Tile>) c ->
        {
            switch (markedTiles.size()) {
                case 1: // first vertex marked
                    System.out.println("v1 marked");
                    break;
                case 2: // second vertex marked
                    System.out.println("v2 marked");
                    BoardAnimator.stopTransitions(board, dijkstrasPathFinder.getVertices());
                    isInMarkMode.set(false);
                    break;
                case 0: // list cleared
                    System.out.println("marked vertices cleared");
                    break;
            }
        });
    }

    public BoardControllerTab(String boardName, DoubleExpression cellSize, BoardStructure boardStructure, TileType... allowedTileTypes) {
        this(boardName, new Board(cellSize, boardStructure), new DijkstrasPathFinder(boardStructure, allowedTileTypes));
    }

    public Board getBoard() {
        return board;
    }

    public DijkstrasPathFinder getDijkstrasPathFinder() {
        return dijkstrasPathFinder;
    }

    public ObservableList<Tile> getMarkedTiles() {
        return markedTiles;
    }

    public boolean isIsInMarkMode() {
        return isInMarkMode.get();
    }

    public SimpleBooleanProperty isInMarkModeProperty() {
        return isInMarkMode;
    }
}
