package com.gyarmatip.pacmanedu.ui.main;

import com.gyarmatip.boardapi.board.Board;
import com.gyarmatip.boardapi.board.BoardStructure;
import com.gyarmatip.boardapi.board.tile.Tile;
import com.gyarmatip.boardapi.board.tile.TileType;
import com.gyarmatip.boardapi.navigation.pathfinding.DijkstrasPathFinder;
import com.gyarmatip.pacmanedu.tilesets.PacmanTile;
import javafx.beans.binding.DoubleExpression;
import javafx.beans.property.SimpleObjectProperty;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;

import java.net.URL;
import java.util.ResourceBundle;

public class BoardViewController implements Initializable {

    @FXML private TabPane boardTabPane;
    private SimpleObjectProperty<BoardControllerTab> activeBoardControllerTab = new SimpleObjectProperty<>();

    // txt path to default board
    private final static String pacmanBoardTxtPath = "src/main/resources/com/gyarmatip/pacmanedu/pacman_board.txt";

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        BoardControllerTab defaultBoardControllerTab = createDefaultBoardControllerTab();
        addBoardControllerTab(defaultBoardControllerTab);
        // listen to tab change
        boardTabPane.getSelectionModel().selectedItemProperty().addListener((observable, oldValue, newValue) ->
        {
            if (newValue instanceof BoardControllerTab) {
                BoardControllerTab boardControllerTab = (BoardControllerTab)newValue;
                activeBoardControllerTab.set(boardControllerTab);
            }
        });
    }

    private BoardControllerTab createDefaultBoardControllerTab() {
        BoardStructure boardStructure = new BoardStructure(pacmanBoardTxtPath, PacmanTile.class);
        DoubleExpression cellSize = boardTabPane.widthProperty().subtract(10).divide(boardStructure.getNumberOfColumns());
        TileType[] allowedTileTypes = {PacmanTile.FOOD, PacmanTile.BIO_FOOD, PacmanTile.PATH};
        return new BoardControllerTab("Pacman", cellSize, boardStructure, allowedTileTypes);
    }

    public void addTab(Node node, String tabName) {
        ScrollPane scrollPane = new ScrollPane();
        scrollPane.getStyleClass().setAll("board-container");
        scrollPane.setContent(node);
        Tab tab = new Tab(tabName, scrollPane);
        boardTabPane.getTabs().add(tab);
        boardTabPane.getSelectionModel().select(tab);
    }

    public Board getBoard() {
        return activeBoardControllerTab.get().getBoard();
    }

    public DijkstrasPathFinder getDijkstrasPathFinder() {
        return activeBoardControllerTab.get().getDijkstrasPathFinder();
    }

    public ObservableList<Tile> getMarkedTiles() {
        return activeBoardControllerTab.get().getMarkedTiles();
    }

    public TabPane getBoardTabPane() {
        return boardTabPane;
    }

    public BoardControllerTab getActiveBoardControllerTab() {
        return activeBoardControllerTab.get();
    }

    private void addBoardControllerTab(BoardControllerTab boardControllerTab) {
        boardTabPane.getTabs().add(boardControllerTab);
        boardTabPane.getSelectionModel().select(boardControllerTab);
        activeBoardControllerTab.set(boardControllerTab);
    }

    public void changeEditorTab(BoardControllerTab boardControllerTab) {
        int lastIndex = boardTabPane.getTabs().size()-1;
        boardTabPane.getTabs().set(lastIndex, boardControllerTab);
        boardTabPane.getSelectionModel().select(boardControllerTab);
    }

    public void removeSelectedTab() {
        boardTabPane.getTabs().remove(boardTabPane.getSelectionModel().getSelectedItem());
    }

}