package com.gyarmatip.pacmanedu.ui;

import com.jfoenix.controls.JFXProgressBar;
import javafx.concurrent.Task;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;

import java.net.URL;
import java.util.ResourceBundle;

public class SplashViewController implements Initializable {

    @FXML JFXProgressBar progressBar;

    @Override
    public void initialize(URL location, ResourceBundle resources) {

    }

    /**
     * Loads the main UI
     * @param task the Task which loads the main UI
     */
    public void startLoading(Task<Void> task) {
        progressBar.progressProperty().bind(task.progressProperty());
        task.run();
    }

}