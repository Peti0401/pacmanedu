package com.gyarmatip.pacmanedu.ui.sections;

import com.gyarmatip.boardapi.board.BoardStructure;
import com.gyarmatip.boardapi.board.tile.Tile;
import com.gyarmatip.boardapi.board.tile.TileType;
import com.gyarmatip.boardapi.navigation.pathfinding.DijkstrasPathFinder;
import com.gyarmatip.boardapi.tilesets.TileTypeWrapper;
import com.gyarmatip.pacmanedu.Main;
import com.gyarmatip.pacmanedu.ui.main.BoardControllerTab;
import javafx.beans.binding.DoubleExpression;
import javafx.beans.property.BooleanProperty;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.geometry.HPos;
import javafx.geometry.VPos;
import javafx.scene.control.ColorPicker;
import javafx.scene.control.TextField;
import javafx.scene.control.Toggle;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.util.Pair;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.stream.Collectors;

import static com.gyarmatip.pacmanedu.Controllers.boardViewController;

public class GraphBuilderViewController implements Initializable {

    @FXML private TextField graphNameField;
    @FXML private ColorPicker baseColorPicker;
    @FXML private ColorPicker highlightColorPicker;
    @FXML private TextField characterTextField;
    private int charCount;
    @FXML private Toggle isAllowedToggle;

    @FXML private TextField numRowsField;
    @FXML private TextField numColsField;

    @FXML private FlowPane customTilesContainer;
    @FXML private StackPane selectedTileContainer;
    private SimpleObjectProperty<Tile> selectedTile;
    private List<Pair<TileType, Boolean>> tileTypes;

    private BoardStructureEditor editor;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        initAll();
    }

    private void initAll() {
        charCount = 0;
        selectedTile = new SimpleObjectProperty<>();
        tileTypes = new ArrayList<>();
        characterTextField.setText(String.valueOf(charCount++));
        selectedTileContainer.getChildren().clear();
        customTilesContainer.getChildren().clear();
        graphNameField.clear();
        baseColorPicker.setValue(Color.GREEN);
        highlightColorPicker.setValue(Color.RED);
    }

    /**
     * Extract TileType values from UI
     * @param event
     */
    @FXML
    private void createTile(ActionEvent event) {
        // extracted tile type from ui
        char character = characterTextField.getText().charAt(0);
        Color baseColor = baseColorPicker.getValue();
        Color highlightColor = highlightColorPicker.getValue();
        Color opacityReducedHighlightColor = Color.rgb(
                ((int) highlightColor.getRed()),
                ((int) highlightColor.getGreen()),
                ((int) highlightColor.getBlue()),
                0.55);
        TileType tileType = new TileTypeWrapper(character, baseColor, "", "", opacityReducedHighlightColor);
        // save to list in pair indicating whether it is allowed tile type or not
        tileTypes.add(new Pair<>(tileType, isAllowedToggle.isSelected()));

        DoubleExpression cellSize = customTilesContainer.prefWrapLengthProperty().divide(5);
        final Tile tile = new Tile(tileType, cellSize);
        tile.setOnMouseClicked(e ->
        {
            selectedTile.set(tile); // make selected
            selectedTileContainer.getChildren().setAll(new Tile(tile)); // move a clone to the selected container
        });

        customTilesContainer.getChildren().add(tile); // add the tile to the container
        selectedTileContainer.getChildren().setAll(new Tile(tile)); // move a clone to the selected container
        selectedTile.set(tile); // save to property
        characterTextField.setText(String.valueOf(charCount++)); // update chat field
    }

    /**
     * Create the editor
     * @param event
     */
    @FXML
    private void startBoardBuilding(ActionEvent event) {
        int numRows = 15;
        int numCols = 15;
        try {
            numRows = Integer.parseInt(numRowsField.getText());
            numCols = Integer.parseInt(numColsField.getText());
        } catch (Exception ignore) {}
        editor = new BoardStructureEditor(numRows, numCols, tileTypes);
        boardViewController.addTab(editor, graphNameField.getText());
    }

    /**
     * Extract BoardControllerTab from UI and editor, then reset all
     * @param event
     */
    @FXML
    private void finalizeBoard(ActionEvent event) {
        editor.fillRemainingWithWall();

        String graphName = graphNameField.getText();
        BoardStructure boardStructure = editor.extractBoardStructure();
        DoubleExpression cellSize =
                boardViewController
                .getBoardTabPane()
                .widthProperty()
                .subtract(10)
                .divide(boardStructure.getNumberOfColumns());
        TileType[] allowedTileTypes = editor.getAllowedTileTypes();

        BoardControllerTab boardControllerTab = new BoardControllerTab(graphName, cellSize, boardStructure, allowedTileTypes);
        boardViewController.changeEditorTab(boardControllerTab);

        initAll();
    }

    @FXML
    private void resetTileSet(ActionEvent event) {
        initAll();
        boardViewController.removeSelectedTab();
    }

    private class EditorCell extends StackPane {
        EditorCell() {
            super();
        }

        Tile getTile() {
            return getChildren().isEmpty() ? null : (Tile)getChildren().get(0);
        }
    }

    private class BoardStructureEditor extends GridPane {

        private List<Pair<TileType, Boolean>> tileTypes;
        private BooleanProperty isShiftPressed = new SimpleBooleanProperty();
        private BooleanProperty isDPressed = new SimpleBooleanProperty();
        private DoubleExpression cellSize;
        private int numRows;
        private int numCols;

        BoardStructureEditor(int numRows, int numCols, List<Pair<TileType, Boolean>> tileTypes) {
            super();
            this.numRows = numRows;
            this.numCols = numCols;
            this.tileTypes = new ArrayList<>(tileTypes);
            setGridLinesVisible(true);
            cellSize = boardViewController.getBoardTabPane().widthProperty().subtract(10).divide(numCols);

            for (int i = 0; i < numRows; i++) {
                RowConstraints row = new RowConstraints(0, 0, 0, Priority.NEVER, VPos.CENTER, true);
                row.prefHeightProperty().bind(cellSize);
                row.minHeightProperty().bind(cellSize);
                row.maxHeightProperty().bind(cellSize);
                getRowConstraints().add(row);
            }

            for (int i = 0; i < numCols; i++) {
                ColumnConstraints column = new ColumnConstraints(0, 0, 0, Priority.NEVER, HPos.CENTER, true);
                column.prefWidthProperty().bind(cellSize);
                column.minWidthProperty().bind(cellSize);
                column.maxWidthProperty().bind(cellSize);
                getColumnConstraints().add(column);
            }

            for (int r = 0; r < numRows; r++) {
                for (int c = 0; c < numCols; c++) {
                    EditorCell pane = new EditorCell();
                    pane.prefWidthProperty().bind(cellSize);
                    pane.prefHeightProperty().bind(cellSize);
                    pane.minWidthProperty().bind(cellSize);
                    pane.minHeightProperty().bind(cellSize);
                    pane.maxWidthProperty().bind(cellSize);
                    pane.maxHeightProperty().bind(cellSize);
                    pane.setOnMouseClicked(event -> pane.getChildren().setAll(new Tile(selectedTile.get(), cellSize)));
                    pane.setOnMouseMoved(event ->
                    {
                        if (isShiftPressed.get()) {
                            if (isDPressed.get()) {
                                pane.getChildren().clear();
                            } else {
                                pane.getChildren().setAll(new Tile(selectedTile.get(), cellSize));
                            }
                        }
                    });
                    pane.setOnMouseClicked(event ->
                    {
                        if (isDPressed.get()) {
                            pane.getChildren().clear();
                        } else {
                            pane.getChildren().setAll(new Tile(selectedTile.get(), cellSize));
                        }
                    });
                    add(pane, c, r);
                }
            }

            Main.mainScene.setOnKeyPressed(event ->
            {
                if (event.getCode() == KeyCode.SHIFT) {
                    isShiftPressed.setValue(true);
                } else if (event.getCode() == KeyCode.D) {
                    isDPressed.setValue(true);
                }
            });

            Main.mainScene.setOnKeyReleased(event ->
            {
                if (event.getCode() == KeyCode.SHIFT) {
                    isShiftPressed.setValue(false);
                } else if (event.getCode() == KeyCode.D) {
                    isDPressed.setValue(false);
                }
            });

        }

        private void fillRemainingWithWall() {
            Optional<Pair<TileType, Boolean>> tileTypeOptional = tileTypes.stream()
                    .filter(tileTypeBooleanPair -> !tileTypeBooleanPair.getValue())
                    .findAny();
            Tile tile = tileTypeOptional
                    .map(tileTypeBooleanPair -> new Tile(tileTypeBooleanPair.getKey(), cellSize))
                    .orElseGet(() -> new Tile(tileTypes.get(0).getKey(), cellSize));
            List<StackPane> emptyPanes = getChildren().stream()
                    .filter(node -> node instanceof StackPane)
                    .map(node -> (StackPane)node)
                    .filter(stackPane -> stackPane.getChildren().isEmpty())
                    .collect(Collectors.toList());
            emptyPanes.forEach(stackPane -> stackPane.getChildren().setAll(new Tile(tile, cellSize)));
        }

        private TileType[][] getTileTypeGrid() {
            TileType[][] tileTypeGrid = new TileType[numRows][numCols];
            List<EditorCell> editorCells = getChildren()
                    .stream()
                    .filter(node -> node instanceof EditorCell)
                    .map(node -> (EditorCell)node)
                    .collect(Collectors.toList());
            editorCells.forEach(cell ->
            {
                Tile tile = cell.getTile();
                int r = GridPane.getRowIndex(cell);
                int c = GridPane.getColumnIndex(cell);
                tileTypeGrid[r][c] = tile.getTileType();
            });
            return tileTypeGrid;
        }

        private TileType[] getTileTypeConstants() {
            return tileTypes.stream()
                    .map(Pair::getKey)
                    .toArray(TileType[]::new);
        }

        private TileType[] getAllowedTileTypes() {
            return tileTypes.stream()
                    .filter(Pair::getValue)
                    .map(Pair::getKey)
                    .toArray(TileType[]::new);
        }

        private BoardStructure extractBoardStructure() {
            return new BoardStructure(getTileTypeGrid(), getTileTypeConstants());
        }

        private DijkstrasPathFinder getDijkstrasPathFinder() {
            return new DijkstrasPathFinder(extractBoardStructure(), getAllowedTileTypes());
        }

    }

}