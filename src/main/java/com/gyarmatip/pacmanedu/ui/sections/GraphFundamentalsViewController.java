package com.gyarmatip.pacmanedu.ui.sections;

import com.gyarmatip.boardapi.board.Board;
import com.gyarmatip.boardapi.board.BoardAnimator;
import com.gyarmatip.boardapi.graph.Vertex;
import com.gyarmatip.boardapi.navigation.Position;
import com.gyarmatip.boardapi.navigation.pathfinding.DijkstrasPathFinder;
import com.gyarmatip.pacmanedu.Controllers;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Label;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.util.Duration;
import org.jgrapht.Graph;
import org.jgrapht.graph.DefaultWeightedEdge;

import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.ResourceBundle;

public class GraphFundamentalsViewController implements Initializable {

    @FXML private TabPane tabPane;
    @FXML private Tab vertexTab;
    @FXML private Tab edgeTab;
    @FXML private Tab edgeWeightTab;


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        tabPane.getSelectionModel().selectedItemProperty()
                .addListener((observable, oldValue, newValue) ->
                        BoardAnimator.clearBoard(Controllers.boardViewController.getBoard()));
    }

    @FXML
    private void visualizeSelection(ActionEvent event) {
        Board board = Controllers.boardViewController.getBoard();
        DijkstrasPathFinder pathFinder = Controllers.boardViewController.getDijkstrasPathFinder();
        Graph<Vertex, DefaultWeightedEdge> graph = pathFinder.getGraph();
        Random rand = new Random();

        BoardAnimator.clearBoard(board);
        if (vertexTab.isSelected()) { // show a random vertex
            List<Vertex> vertices = new ArrayList<>(graph.vertexSet());
            int randIndex = rand.nextInt(vertices.size());
            Vertex randomVertex = vertices.get(randIndex);

            BoardAnimator.blinkTile(board, Duration.millis(1000), randomVertex);
        } else if (edgeTab.isSelected()) { // show a random edge
            List<DefaultWeightedEdge> edges = new ArrayList<>(graph.edgeSet());
            int randIndex = rand.nextInt(edges.size());
            DefaultWeightedEdge randomEdge = edges.get(randIndex);

            Vertex src = graph.getEdgeSource(randomEdge);
            Vertex target = graph.getEdgeTarget(randomEdge);
            List<Position> inBetweenPositions = src.getPositionInterval(target,src.getDirectionTo(target), true, true, pathFinder);

            BoardAnimator.animatePath(board, Duration.millis(250).multiply(inBetweenPositions.size()), inBetweenPositions);
        } else if (edgeWeightTab.isSelected()) { // show a random weight on an edge
            List<DefaultWeightedEdge> edges = new ArrayList<>(graph.edgeSet());
            int randIndex = rand.nextInt(edges.size());
            DefaultWeightedEdge randomEdge = edges.get(randIndex);

            Vertex src = graph.getEdgeSource(randomEdge);
            Vertex target = graph.getEdgeTarget(randomEdge);
            List<Position> inBetweenPositions = src.getPositionInterval(target,src.getDirectionTo(target), true, true, pathFinder);
            int weight = inBetweenPositions.size();
            for (int i = 0; i < inBetweenPositions.size(); i++) {
                Label label = board.getTile(inBetweenPositions.get(i)).getLabel();
                label.setText("" + (i+1));
            }

            BoardAnimator.animatePath(board, Duration.millis(250).multiply(inBetweenPositions.size()), inBetweenPositions);
        }
    }

}