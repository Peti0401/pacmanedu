package com.gyarmatip.pacmanedu.ui.sections;

import animatefx.animation.AnimationFX;
import com.gyarmatip.boardapi.board.Board;
import com.gyarmatip.boardapi.board.BoardAnimator;
import com.gyarmatip.boardapi.board.tile.Tile;
import com.gyarmatip.boardapi.graph.Vertex;
import com.gyarmatip.boardapi.navigation.Position;
import com.gyarmatip.boardapi.navigation.pathfinding.DijkstrasPathFinder;
import com.gyarmatip.pacmanedu.algorithmobjects.Algorithm;
import com.gyarmatip.pacmanedu.algorithmobjects.AlgorithmFactory;
import com.gyarmatip.pacmanedu.utils.Utils;
import com.jfoenix.controls.JFXSnackbar;
import javafx.beans.property.SimpleBooleanProperty;
import javafx.collections.FXCollections;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.*;
import javafx.util.Callback;
import javafx.util.Duration;
import javafx.util.Pair;
import org.jgrapht.Graph;
import org.jgrapht.GraphPath;
import org.jgrapht.alg.cycle.HierholzerEulerianCycle;
import org.jgrapht.alg.spanning.KruskalMinimumSpanningTree;
import org.jgrapht.graph.DefaultWeightedEdge;

import java.net.URL;
import java.util.*;

import static com.gyarmatip.pacmanedu.Controllers.boardViewController;
import static com.gyarmatip.pacmanedu.Controllers.mainViewController;
import static com.gyarmatip.pacmanedu.algorithmobjects.AlgorithmFactory.*;

public class AdvancedAlgorithmsViewController implements Initializable {

    @FXML private ComboBox<Algorithm> algorithmsComboBox;

    @FXML private TextArea descriptionTextArea;
    @FXML private TextArea stepsTextArea;
    @FXML private TextArea furtherUsesTextArea;

    @FXML private Button pickVerticesBtn;
    @FXML private Button runAlgorithmBtn;
    @FXML private Button interpretResultBtn;

    @FXML private Label algorithmActionLabel;
    private SimpleBooleanProperty isAnimationRunning = new SimpleBooleanProperty(false);

    private ResourceBundle resourceBundle;

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        initComboBox();
        resourceBundle = resources;
    }

    private void initComboBox() {
        algorithmsComboBox.setItems(FXCollections.observableList(AlgorithmFactory.getDeclaredAlgorithms()));
        Callback<ListView<Algorithm>, ListCell<Algorithm>> cellFactory = new Callback<ListView<Algorithm>, ListCell<Algorithm>>() {
            @Override
            public ListCell<Algorithm> call(ListView<Algorithm> param) {
                return new ListCell<Algorithm>(){
                    @Override
                    protected void updateItem(Algorithm item, boolean empty) {
                        super.updateItem(item, empty);
                        if (item == null || empty) {
                            setGraphic(null);
                        } else {
                            setText(item.getName());
                        }
                    }
                };
            }
        };
        algorithmsComboBox.setButtonCell(cellFactory.call(null));
        algorithmsComboBox.setCellFactory(cellFactory);

        // listen for combo box value change and update text areas
        algorithmsComboBox.valueProperty().addListener((observable, oldValue, newValue) ->
        {
            updateTextAreas(newValue);
            pickVerticesBtn.setDisable(!newValue.equals(DIJKSTRA));
            runAlgorithmBtn.setDisable(!pickVerticesBtn.isDisable());
            interpretResultBtn.setDisable(true);
            BoardAnimator.clearBoard(boardViewController.getBoard());
            algorithmActionLabel.setText("");
        });

        // trigger update
        algorithmsComboBox.getSelectionModel().select(0);
    }

    private void updateTextAreas(Algorithm algorithm) {
        descriptionTextArea.setText(algorithm.getDescription());
        stepsTextArea.setText(algorithm.getAlgorithmSteps());
        furtherUsesTextArea.setText(algorithm.getFurtherUses());
    }

    @FXML
    private void pickVertices(ActionEvent event) {
        algorithmActionLabel.setText("");
        Algorithm currentAlgorithm = algorithmsComboBox.getValue();
        JFXSnackbar snackbar = mainViewController.getSnackbar();

        Board board = boardViewController.getBoard();
        DijkstrasPathFinder dijkstrasPathFinder = boardViewController.getDijkstrasPathFinder();

        JFXSnackbar.SnackbarEvent snackbarEvent = new JFXSnackbar.SnackbarEvent(
                currentAlgorithm.getActionDescription(),
                "Ok",
                3000,
                true,
                e ->
                {
                    snackbar.close();
                    boardViewController.getActiveBoardControllerTab().isInMarkModeProperty().set(true);
                    BoardAnimator.blinkTiles(board, Duration.millis(1500), dijkstrasPathFinder.getVertices());
                    pickVerticesBtn.setDisable(true);
                    runAlgorithmBtn.setDisable(false);
                });

        snackbar.fireEvent(snackbarEvent);
    }

    @FXML
    private void runAlgorithm(ActionEvent event) {
        Graph<Vertex, DefaultWeightedEdge> graph = boardViewController.getDijkstrasPathFinder().getGraph();
        algorithmActionLabel.setText("");
        runAlgorithmBtn.setDisable(true);

        Algorithm currentAlgorithm = algorithmsComboBox.getValue();
        List<Position> positionsToHighlight = new ArrayList<>();

        if (currentAlgorithm.equals(DIJKSTRA)) {

            try {
                Tile t1 = boardViewController.getMarkedTiles().get(0);
                Position p1 = boardViewController.getBoard().getPosition(t1);

                Tile t2 = boardViewController.getMarkedTiles().get(1);
                Position p2 = boardViewController.getBoard().getPosition(t2);

                Position[] pathPositions = boardViewController.getDijkstrasPathFinder().findPath(p1, p2);
                positionsToHighlight.addAll(Arrays.asList(pathPositions));
            } catch (Exception e) {
                e.printStackTrace();
                String errorMsg = resourceBundle.getString("key.error1");
                Utils.AnimationUtil.animateLabelText(algorithmActionLabel, errorMsg)[0].play();
                return;
            }

        } else if (currentAlgorithm.equals(EULERIAN)) {
            try {
                HierholzerEulerianCycle eulerianCycle = new HierholzerEulerianCycle();
                GraphPath<Vertex, DefaultWeightedEdge> path = eulerianCycle.getEulerianCycle(boardViewController.getDijkstrasPathFinder().getGraph());
                List<Vertex> vertices = path.getVertexList();
                for (int i = 0; i < vertices.size() - 1; i++) {
                    Vertex cur = vertices.get(i);
                    Vertex next = vertices.get(i+1);
                    List<Position> positionInterval = cur.getPositionInterval(next, cur.getDirectionTo(next), true, true, boardViewController.getDijkstrasPathFinder());
                    positionsToHighlight.addAll(positionInterval);
                }
            } catch (Exception e) {
                e.printStackTrace();
                String errorMsg = resourceBundle.getString("key.error2");
                Utils.AnimationUtil.animateLabelText(algorithmActionLabel, errorMsg)[0].play();
                return;
            }

        }
        /*else if (currentAlgorithm.equals(HAMILTONIAN)) {
            try {

            } catch (Exception e) {
                e.printStackTrace();
                String errorMsg = resourceBundle.getString("key.error3");
                Utils.AnimationUtil.animateLabelText(algorithmActionLabel, errorMsg)[0].play();
                return;
            }
        }*/
        else if (currentAlgorithm.equals(KRUSKAL)) {
            try {
                KruskalMinimumSpanningTree<Vertex, DefaultWeightedEdge> kruskal = new KruskalMinimumSpanningTree<>(graph);
                Set<DefaultWeightedEdge> edges = kruskal.getSpanningTree().getEdges();
                for (DefaultWeightedEdge edge : edges) {
                    Vertex src = graph.getEdgeSource(edge);
                    Vertex target = graph.getEdgeTarget(edge);
                    List<Position> positionInterval = src.getPositionInterval(target, src.getDirectionTo(target), true, true, boardViewController.getDijkstrasPathFinder());
                    positionsToHighlight.addAll(positionInterval);
                }
            } catch (Exception e) {
                e.printStackTrace();
                String errorMsg = resourceBundle.getString("key.error4");
                Utils.AnimationUtil.animateLabelText(algorithmActionLabel, errorMsg)[0].play();
                return;
            }
        }

        Pair<AnimationFX[], Duration> pair = Utils.AnimationUtil.animateLabelTexts(algorithmActionLabel, currentAlgorithm.getSingleAlgorithmSteps());
        AnimationFX[] animations = pair.getKey();
        Duration fullDuration = pair.getValue();
        animations[1].setOnFinished(e ->
        {
            isAnimationRunning.set(false);
            algorithmActionLabel.setText("");
            interpretResultBtn.setDisable(false);
        });
        isAnimationRunning.set(true);
        positionsToHighlight = new ArrayList<>(new HashSet<>(positionsToHighlight)); // remove duplicates
        BoardAnimator.animatePath(boardViewController.getBoard(), fullDuration, positionsToHighlight);
        animations[0].play();
    }

    @FXML
    private void interpretResult(ActionEvent event) {
        Algorithm currentAlgorithm = algorithmsComboBox.getValue();
        interpretResultBtn.setDisable(true);
        pickVerticesBtn.setDisable(!currentAlgorithm.equals(DIJKSTRA));
        runAlgorithmBtn.setDisable(!pickVerticesBtn.isDisable());
        boardViewController.getMarkedTiles().clear();
        BoardAnimator.clearBoard(boardViewController.getBoard());
        AnimationFX[] animations = Utils.AnimationUtil.animateLabelText(algorithmActionLabel, currentAlgorithm.getResultInterpretation());
        animations[0].play();
    }

}