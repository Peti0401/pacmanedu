package com.gyarmatip.pacmanedu;

import animatefx.animation.AnimationFX;
import animatefx.animation.FlipOutY;
import com.gyarmatip.pacmanedu.ui.SplashViewController;
import com.gyarmatip.pacmanedu.ui.main.MainViewController;
import com.gyarmatip.pacmanedu.utils.UTF8Control;
import com.gyarmatip.pacmanedu.utils.Utils;
import javafx.application.Application;
import javafx.concurrent.Task;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.paint.Color;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.util.Duration;
import javafx.util.Pair;

import java.util.Locale;
import java.util.ResourceBundle;

public class Main extends Application {

    public static Locale usedLocale = Locale.forLanguageTag("hu");
    private Stage splashStage;
    private Stage mainStage;
    public static Scene mainScene;

    @Override
    public void start(Stage primaryStage) throws Exception {
        initSplashStage();
        splashStage.show();
        Controllers.splashViewController.startLoading(createMainLoadingTask());
    }

    public static void main(String[] args) {
        launch(args);
    }

    private void initSplashStage() {
        Pair<Node, Initializable> splashPair = Utils.loadFXML("ui/SplashView.fxml", getClass());
        Parent root = ((Parent) splashPair.getKey());
        Controllers.splashViewController = ((SplashViewController) splashPair.getValue());

        Scene scene = new Scene(root);
        scene.setFill(Color.TRANSPARENT);

        splashStage = new Stage(StageStyle.TRANSPARENT);
        splashStage.setScene(scene);
    }

    private void initMainStage() {
        ResourceBundle mainBundle = ResourceBundle.getBundle("com.gyarmatip.pacmanedu.bundles.main", usedLocale, new UTF8Control());
        Pair<Node, Initializable> mainPair = Utils.loadFXML("ui/main/MainView.fxml", getClass(), mainBundle);
        Parent root = ((Parent) mainPair.getKey());
        Controllers.mainViewController = ((MainViewController) mainPair.getValue());

        mainScene = new Scene(root);
        mainScene.setFill(Color.TRANSPARENT);
        mainScene.getStylesheets().setAll(
                getClass().getResource("generalStyles.css").toExternalForm(),
                getClass().getResource("styles.css").toExternalForm()
        );

        mainStage = new Stage();
        mainStage.setScene(mainScene);
        mainStage.setFullScreen(true);
        mainStage.setResizable(false);
    }

    private Task<Void> createMainLoadingTask() {
        Task<Void> task = new Task<Void>() {
            @Override
            protected Void call() throws Exception {
                initMainStage();
                return null;
            }
        };
        task.setOnSucceeded(event ->
        {
            AnimationFX splashOut = new FlipOutY(splashStage.getScene().getRoot());
            splashOut.getTimeline().setOnFinished(finished ->
            {
                splashStage.close();
                mainStage.show();
            });
            splashOut.setDelay(Duration.millis(0));
            splashOut.play();
        });
        return task;
    }

}
