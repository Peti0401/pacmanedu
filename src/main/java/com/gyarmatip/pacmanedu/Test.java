package com.gyarmatip.pacmanedu;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;

public class Test extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        VBox root = new VBox(10);
        root.setAlignment(Pos.CENTER);
        root.setPadding(new Insets(20));

        Label label = new Label("old");
        Button btn = new Button("animate");

        root.getChildren().addAll(label, btn);
        primaryStage.setScene(new Scene(root, 1080, 720));
        primaryStage.show();

    }
}
