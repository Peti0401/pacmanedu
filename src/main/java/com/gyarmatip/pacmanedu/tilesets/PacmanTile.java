package com.gyarmatip.pacmanedu.tilesets;

import com.gyarmatip.boardapi.board.tile.TileType;
import javafx.scene.paint.Color;
import javafx.scene.paint.Paint;

public enum PacmanTile implements TileType {

    WALL('F', Color.BLUE, null, "", Color.rgb(120, 200, 120, 0.55)),
    GHOST_WALL('G', Color.ALICEBLUE, null, "", Color.rgb(120, 200, 120, 0.55)),
    FOOD('1', Color.WHITE, null, "", Color.rgb(120, 200, 120, 0.55)),
    BIO_FOOD('+', Color.NAVAJOWHITE, null, "", Color.rgb(120, 200, 120, 0.55)),
    PATH('0', Color.BLACK, null, "", Color.rgb(120, 200, 120, 0.55));

    private final Character character;
    private final Paint basePaint;
    private final String imageURL;
    private final String labelText;
    private final Paint highlighterPaint;

    PacmanTile(Character character, Paint basePaint, String imageURL, String labelText, Paint highlighterPaint) {
        this.character = character;
        this.basePaint = basePaint;
        this.imageURL = imageURL;
        this.labelText = labelText;
        this.highlighterPaint = highlighterPaint;
    }

    public Character getCharacter() {
        return character;
    }

    public Paint getBasePaint() {
        return basePaint;
    }

    public String getImageURL() {
        return imageURL;
    }

    public String getLabelText() {
        return labelText;
    }

    public Paint getHighlighterPaint() {
        return highlighterPaint;
    }

}