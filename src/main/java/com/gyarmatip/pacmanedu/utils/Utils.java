package com.gyarmatip.pacmanedu.utils;

import animatefx.animation.AnimationFX;
import animatefx.animation.FlipInX;
import animatefx.animation.FlipOutY;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Node;
import javafx.scene.control.Label;
import javafx.util.Duration;
import javafx.util.Pair;

import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.ResourceBundle;
import java.util.stream.Collectors;

public class Utils {

    public static Pair<Node, Initializable> loadFXML(String fxmlPath, Class loaderClass, ResourceBundle resourceBundle) {
        FXMLLoader loader = new FXMLLoader(loaderClass.getResource(fxmlPath));
        if (resourceBundle != null) {
            loader.setResources(resourceBundle);
        }
        Node node = null;
        try {
            node = loader.load();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Initializable controller = loader.getController();
        return new Pair<>(node, controller);
    }

    public static Pair<Node, Initializable> loadFXML(String fxmlPath, Class loaderClass) {
        return loadFXML(fxmlPath, loaderClass, null);
    }

    /**
     * Utility functions for Java reflections
     */
    public static class ReflectionUtil {

        /**
         * Reflectively returns all declared fields of an object, including inherited ones as well
         * @param obj the object whose fields get returned
         * @param <T>
         * @return a list of Field objects
         */
        public static <T> List<Field> getFields(T obj) {
            // the class whose fields are inspected
            Class clazz = obj.getClass();
            List<Field> fieldList = new ArrayList<>();
            // stepping higher anf higher in the class tree-hierarchy until the last super class, Object class is reached
            while (clazz != Object.class) {
                // appending all the current class' declared fields to the list
                fieldList.addAll(Arrays.asList(clazz.getDeclaredFields()));
                clazz = clazz.getSuperclass();
            }
            return fieldList;
        }

        /**
         * Reflectively returns the value of an object's field by invoking the corresponding getter
         * @param field The field whose values is returned
         * @param obj The object whose field is reflected
         * @param <T>
         * @return The value of the parameter object's field
         */
        public static <T> Object callGetter(Field field, T obj) {
            try {
                // filtering all publicly available methods for the correct getter
                return Arrays
                        .stream(obj.getClass().getMethods())
                        .filter(method -> method.getName().startsWith("get") &&
                                method.getName().length() == (field.getName().length() + "get".length()) &&
                                method.getName().toLowerCase().endsWith(field.getName().toLowerCase()))
                        .toArray(Method[]::new)[0]
                        .invoke(obj); // after obtaining the correct getter, invoke it
            } catch (IllegalAccessException | InvocationTargetException e) {
                e.printStackTrace();
            }
            return null;
        }


        /**
         * Reflectively returns the value of an object's field by invoking the corresponding getter
         * @param field The field whose values is returned
         * @param obj The object whose field is reflected
         * @param <T>
         * @return The value of the parameter object's field
         */
        public static <T> Object callPropertyGetter(Field field, T obj) {
            try {
                // filtering all publicly available methods for the correct getter
                return Arrays
                        .stream(obj.getClass().getMethods())
                        .filter(method -> method.getName().equals(field.getName() + "Property"))
                        .toArray(Method[]::new)[0]
                        .invoke(obj); // after obtaining the correct getter, invoke it
            } catch (IllegalAccessException | InvocationTargetException e) {
                e.printStackTrace();
            }
            return null;
        }
    }

    public static class AnimationUtil {

        public static AnimationFX[] animateLabelText(Label label, String text) {
            AnimationFX out = new FlipOutY(label);
            AnimationFX in = new FlipInX(label);

            out.setOnFinished(event ->
            {
                label.setText(text);
                in.play();
            });
            return new AnimationFX[]{out, in};
        }

        public static Pair<AnimationFX[], Duration> animateLabelTexts(Label label, String... texts) {
            Duration duration = new Duration(0);
            List<AnimationFX[]> animations = Arrays.stream(texts)
                    .map(s -> animateLabelText(label, s))
                    .collect(Collectors.toList());
            for (AnimationFX[] item : animations) {
                for (AnimationFX animationFX : item) {
                    duration = duration.add(animationFX.getTimeline().getTotalDuration());
                }
            }
            for (int i = 0; i < animations.size() - 1; i++) {
                AnimationFX[] cur = animations.get(i);
                AnimationFX[] next = animations.get(i+1);
                cur[1].setOnFinished(event -> next[0].play());
            }
            AnimationFX first = animations.get(0)[0];
            AnimationFX last = animations.get(animations.size() - 1)[1];
            return new Pair<>(new AnimationFX[]{first, last}, duration);
        }

    }

}
